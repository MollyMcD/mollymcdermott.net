---
layout: page
title: About
friendlyTitle: about me
img: ME_Serp.png
permalink: about.html
---

I am a Ph.D. student in the [Safran Lab](http://www.safran-lab.com/) at the University of Colorado Boulder, interested in social and ecological effects on animal behavior. In 2017 I finished my master’s at the University of Alaska Fairbanks researching how climate-driven vegetation change in the Arctic may impact insects and the migratory birds that depend on them for food.


