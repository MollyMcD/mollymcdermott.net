---
layout: page
title: Research
friendlyTitle: Research
img: DSCN0318.jpg
permalink: research.html
---

<!-- ![](/images/file_name.jpg "Optional Hover Text") -->

## [Curriculum vitae - updated March 2020](/mcdermott_cv_full_mar_5_2020.pdf)

## Current research

![](/images/Me_BARS_JC.png)

I am broadly interested in how the environment shapes animal traits used in mate choice and breeding behavior. For my PhD research I am using the Barn Swallow as a model system to understand interactions between sexual selection and social & environmental factors. Driving questions of my current research are:

1. How does the non-breeding environment affect expression of sexually selection traits? Does the influence of environmental factors differ in magnitude among closely related populations?
2. What physiological mechanisms govern responses to life-history tradeoffs when personal or reproductive costs are manipulated?


## Previous research

![](/images/ATSP_caterpillar_willow.png)

### What are the ecological consequences of vegetation change in the boreal-Arctic transition zone?
Warmer summers and thawing permafrost mean big changes in the Arctic. Across the globe, tundra vegetation is being overtaken by shrubby thickets. What does this large-scale change mean for Arctic animals? For my master’s thesis, I worked in association with the [USGS Changing Arctic Ecosystems Initiative](https://pubs.usgs.gov/fs/2011/3136). We studied bird and arthropod communities across a gradient of tundra to shrubby habitats. We found that shrub thickets host a greater abundance and biomass of arthropods, particular herbivores and parasitoids.

> McDermott, M. T., P. Doak, C. M. Handel, G. A. Breed, and C. P. Mulder. Arthropod communities across tundra-shrub ecotones of northwestern Alaska: Implications of continued shrub expansion. Ecosphere – _in prep._

> McDermott, M. T., (2016). New findings of twisted-wing parasites (Strepsiptera) in Alaska.
Newsletter of the Alaska Entomological Society, Vol. 9(1), pp. 6-8

I studied Arctic songbird nestling diet diversity using next-generation sequencing, a non-invasive approach just beginning to be used in wild birds to understand diet composition. Nestlings in our study generally had more diverse diets than temperate songbirds studied with similar methods, indicating potential resilience to changes in food supply. Caterpillars were a favored food for parents to feed nestlings. Common prey items were more abundant in shrub thickets, indicating that continued shrub expansion many increase prey availability, at least in the short term.

> McDermott, M. T., C. M. Handel, P. Doak, and G. A. Breed. High diet diversity of arctic passerine nestlings revealed by next-generation sequencing. Auk – _under internal review at USGS._

As a technician with the U.S. Geological Survey in 2013 and 2014, I helped to replicate surveys done in 1988-1990. Our goal was to assess how populations of migratory birds had changed in western Alaska over 25 years of climate and vegetation change. The authors found that populations of shrub-associated birds were generally increasing, but that many tundra-associated birds were stable or declining.

> Thompson, S. J., C. M. Handel, R. M. Richardson, and L.B. McNew. (2016). When winners become losers: Predicted nonlinear responses of Arctic birds to increasing woody vegetation. PLoS One, 11(11), e0164755.

> Press: [KNOM - "Bird Camp"](http://www.knom.org/wp/blog/2014/06/24/profile-bird-camp/)




