---
layout: page
title: Outreach
friendlyTitle: outreach
img: Side_by_Side4GA_crop.jpg
permalink: outreach.html
---

## Side by side
![](/images/observationstation4_orig.jpg)

Where does culture end, and ecology begin? What can we learn about our current conservation crisis from spaces that are designed to be shared by humans and animals? Collaborator [Aaron Treher](http://www.aarontreher.com) and I sought to address this question by building creative barn swallow habitat and studying visitation rates by swallows from a nearby colony. With support from the NEST Studio for the Arts, Inside the Greenhouse, and the Art & Rural Environments Field School, we put on a public event to bring artists and conservationists together in rural Colorado.

> [CU Boulder Today article](https://www.colorado.edu/today/2018/09/05/pushing-boundaries-grad-students-think-barn-swallows-craft-artistic-nesting-site)


## Alaska Summer Research Academy

![](/images/Picture1.jpg)

Together with gadget wizard extraordinaire, Kina Smith, I co-designed and co-taught a two-week summer camp for high school students. We taught students to build electronic sensors, and use them to conduct their own environmental research project. Student projects included:

 - measuring sound reception ability of different animal ear models
 - automatic detection & air quality characterization of passing traffic
 - measuring air quality near paved/unpaved roads

## CU Museum of Natural History Family Days

As part of the Evolution Outreach Committee at CU, I've helped with several museum outreach days by designing and conducting kids activities, including:

 - exploring vision across the animal kingdom (K-6)
 - hearing the impact of noise pollution on whale song (6-12)

## Visiting scientist & instructor

I've designed and taught activities as a visiting scientist for summer camps and middle and high school classrooms. Please get in touch if you are a K-12 teacher covering one of the following topics - I would be happy to coordinate a classroom visit!

 - Bird ecology (example activity: nest building)
 - Insect ecology (example activity: pitfall trapping & identifying insect biodiversity)
 - Evolution / Natural selection (example activity: evolving paper planes)
 - Sexual selection / mating systems (example activity: card genotypes & assortative mating)