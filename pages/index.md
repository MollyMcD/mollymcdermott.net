---
layout: page
title: Home
friendlyTitle: about me
img: ME_Serp.png
permalink: index.html
---

I am a Ph.D. student in the [Safran Lab](http://www.safran-lab.com/) at the University of Colorado Boulder interested in ecological influences on sexual selection. I study how the non-breeding environment and migratory behavior shape differences among divergent subspecies of the Barn Swallow. In 2017 I finished my master’s in ecology at the University of Alaska Fairbanks under [Dr. Pat Doak](http://www.iab.uaf.edu/people/pat_doak/) and in collaboration with the U.S. Geological Survey, researching how climate-driven vegetation change in the Arctic may impact insects and the migratory birds that depend on them for food.

I am also a cellist and singer, and have toured the US and Europe as a member of several bands. In part due to my background as a musician, I am excited about communicating science using art and technology. I've written and performed compositions to illustrate scientific topics, and I recently collaborated with a sculptor to build & monitor creative barn swallow habitat.